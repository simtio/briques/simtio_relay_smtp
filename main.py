from core.baseplugin import BasePlugin
from core.menuitem import MenuItem
from core.page import Page
from core.docker_manager import DockerManager
from bottle import request
from core.config import Config
import time
from core.wizard import Wizard
from core.pluginmanager import PluginManager


class Plugin(BasePlugin):
    def __init__(self):
        super(Plugin, self).__init__()
        self.register_menu_item(MenuItem(self.strings.brick_name, 'fas fa-mail-bulk', "/plugins/smtp_relay/"))

        self.register_page(Page('', Page.METHOD_GET, self.action_index))
        self.register_page(Page('', Page.METHOD_POST, self.action_index_process))

        self.dns_plugin = ("dns" in Config().get("plugins"))

        if self.dns_plugin:
            default_hostname = "smtp." + Config().get("plugins")['dns']['dns_domain']
        else:
            default_hostname = "smtp"
        self.register_wizard(Wizard(
            config_key="hostname",
            question=self.strings.wizard_hostname,
            response_type=Wizard.TYPE_STRING,
            parent=None,
            parent_response=None,
            default=default_hostname
        ))
        self.register_wizard(Wizard(
            config_key="relayhost",
            question=self.strings.wizard_host,
            response_type=Wizard.TYPE_STRING,
            parent=None,
            parent_response=None,
            default=None
        ))
        self.register_wizard(Wizard(
            config_key="relayport",
            question=self.strings.wizard_port,
            response_type=Wizard.TYPE_NUMBER,
            parent=None,
            parent_response=None,
            default=25
        ))
        self.register_wizard(Wizard(
            config_key="has_auth",
            question=self.strings.wizard_auth,
            response_type=Wizard.TYPE_BOOL,
            parent=None,
            parent_response=None,
            default=None
        ))
        self.register_wizard(Wizard(
            config_key="relayuser",
            question=self.strings.wizard_user,
            response_type=Wizard.TYPE_STRING,
            parent="has_auth",
            parent_response=True,
            default=None
        ))
        self.register_wizard(Wizard(
            config_key="relaypassword",
            question=self.strings.wizard_password,
            response_type=Wizard.TYPE_PASSWORD,
            parent="has_auth",
            parent_response=True,
            default=None
        ))

    def action_index(self, msg=None):
        return self.render("index", {"msg": msg,
                                     "hostname": self.config.get("hostname", ""),
                                     "relayhost": self.config.get("relayhost", ""),
                                     "relayport": self.config.get("relayport", ""),
                                     "relayuser": self.config.get("relayuser", "")})

    def action_index_process(self):
        request.body.read()
        print(request.forms.keys())

        if "hostname" in request.forms.keys():
            self.config.set("hostname", request.forms.get('hostname'))
        if "relayhost" in request.forms.keys():
            self.config.set("relayhost", request.forms.get('relayhost'))
        if "relayport" in request.forms.keys():
            self.config.set("relayport", request.forms.get('relayport'))
        if "relayuser" in request.forms.keys():
            self.config.set("relayuser", request.forms.get('relayuser'))
        if "relaypassword" in request.forms.keys():
            if request.forms.get('relaypassword'):
                self.config.set("relaypassword", request.forms.get('relaypassword'))

        if self.deploy():
            r = self.strings.success
        else:
            r = self.strings.error
        return self.action_index(r)

    def deploy(self):
        if DockerManager().has_stack(self.get_plugin_type() + "_" + self.get_plugin_name()):
            DockerManager().remove_stack(self.get_plugin_type() + "_" + self.get_plugin_name())
            time.sleep(20)
        DockerManager().set_secret("simtio_smtp_relay_relayhost_password", self.config.get("relaypassword", "None"))
        return super().deploy()

    def install(self):
        if PluginManager().has_plugin('dns'):
            if self.config.get('hostname') == "smtp." + Config().get('plugins')['dns']['dns_domain']:
                PluginManager().get_plugin('plugins.dns').add_entry(
                    "smtp",
                    "simtio." + Config().get('plugins')['dns']['dns_domain'], "CNAME")
        self.deploy()

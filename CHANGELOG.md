# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.7] - 2020-04-17
### Fixed
- Docker image name

## [1.0.6] - 2020-04-17
### Fixed
- Empty password crash on setup [#3](https://gitlab.com/simtio/briques/simtio_relay_smtp/-/issues/3)

## [1.0.5] - 2020-04-16
### Added
- description fr translation

## [1.0.4] - 2019-12-20
### Fixed
- gulp dependencies

## [1.0.3] - 2019-12-20
### Added
- CI
- Wizard
### Changed
- dynamic version in docker-compose
- Fix dockerfile FROM tag

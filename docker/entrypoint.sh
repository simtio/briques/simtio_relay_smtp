#!/bin/sh

# Get out if variable is not initialized
set -e

# Set path
PATH=/bin:/sbin:/usr/bin:/usr/sbin

# Setup postfix configurations
cp /master.cf /etc/postfix/master.cf
cp /main.cf /etc/postfix/main.cf
echo "myhostname = ${POSTFIX_HOSTNAME}" >> /etc/postfix/main.cf
echo "relayhost = ${POSTFIX_RELAY_HOST}" >> /etc/postfix/main.cf

if [ -n ${POSTFIX_RELAY_USER} ]; then
  echo "smtp_sasl_security_options = noanonymous
smtp_sasl_auth_enable = yes
smtp_sasl_password_maps = hash:/etc/postfix/sasl_password_map" >> /etc/postfix/main.cf
  echo "${POSTFIX_RELAY_HOST}    ${POSTFIX_RELAY_USER}:$(cat /run/secrets/postfix_relay_password)" >> /etc/postfix/sasl_password_map
  chown root: /etc/postfix/sasl_password_map
  postmap /etc/postfix/sasl_password_map
  chmod 0600 /etc/postfix/sasl_password_map
fi


# Create /var/log/mail.log
touch /var/log/mail.log && chown root:adm /var/log/mail.log && chmod 0644 /var/log/mail.log

# Start rsyslog and postfix
service rsyslog start
service postfix start

# Set trap to stop the script proprely when a docker stop is executed
trap : TERM INT

# Tail the mail.log to get output and keep container alive
tail -F /var/log/mail.log

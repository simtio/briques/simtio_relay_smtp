# SIMTIO - SMTP Relay

## Starting the container & Configuring
You can use environnement variables for configuring a very simple relay:

```
Create your secret for relay user password :
echo "P@55w0rd" | docker secret create postfix_relay_password

docker swarm init

docker service create -e "POSTFIX_HOSTNAME=localhost" -e "POSTFIX_RELAY_HOST=[smtp.gmail.com]:587" -e "POSTFIX_RELAY_USER=email@mydomain.com" -p 25:25 -p 587:587 --secret postfix_relay_password --name=simtio-smtp-relay registry.gitlab.com/simtio/briques/simtio_relay_smtp
```

You can also use volumes if you want to manage manually postfix, or store logs. You need to restart the container after postfix configuration changes.

`docker logs -f simtio-smtp-relay` will display the mail logs.

## Dev
```
# Gitlab
docker build -t registry.gitlab.com/simtio/briques/simtio_relay_smtp:latest -t registry.gitlab.com/simtio/briques/simtio_relay_smtp:0.x .
docker push registry.gitlab.com/simtio/briques/simtio_relay_smtp && docker push registry.gitlab.com/simtio/briques/simtio_relay_smtp:0.x

# Dockerhub Multiarch
docker buildx build -t simtio/relay_smtp:latest -t simtio/relay_smtp:0.x --platform=linux/aarch64,linux/amd64,linux/arm . --push
docker push simtio/relay_smtp:latest && docker push simtio/relay_smtp:0.x

docker run --rm -p 25:25 --name=simtio-smtp-relay registry.gitlab.com/simtio/briques/simtio_relay_smtp
```

## Sources
- http://www.postfix.org/
- https://github.com/fw8/docker-postfix
- https://git.imperium-gaming.fr/sysadmins/role-awh-postfix
- https://gist.github.com/sudo-bmitch/f91a943174d6aff5a57904485670a9eb
- http://www.postfix.org/COMPATIBILITY_README.html#chroot
- http://wiki.csnu.org/index.php/Postfix_en_tant_que_relay
